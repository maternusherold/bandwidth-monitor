# Bandwidth Monitor

slim application to monitor internet bandwidth and displaying it using Grafana dashboard.


## Installation & Deployment

There are two major ways to save the gathered data. The simple approach is to 
save the measurements to a, e.g. `.csv`, file. The more robust approach is to 
use [InfluxDB](https://docs.influxdata.com/influxdb/v1.7/). Note, one can provide 
a custom method to write the gathered stats. Such a custom method has to be provided 
to object instantiation as callable under the `measurement_writing_function` 
attribute. 

### Required packages

- `pip install speedtest-cli`


### Requirements for file-base usage

- either use the default writing method, which will save to `bandwidth_stats.csv` 
or provide a custom one

### Requirements for data base usage

- installation of InfluxDB and a data base to write to; i.e. `CREATE DATABASE
 bandwithmonitor` with a user holding at least write access to it
- `pip install influxdb` or `sudo apt install python3-influxdb` 
- set database access in `config.ini`
- check if it's working by running (should display all measurements):
```bash
influx
USE <db-name>
SELECT * FROM <measurement>
```


## Monitoring

- specify the desired measure intervals and either run `monitor.py` as service, 
in a detached or some style which keeps it alive
- **NOTE:** as it writes to the same measurement in InfluxDB it won't crush the 
whole data gathering and it can be restarted easily. Same holds for the default 
writing method as it appends to a provided file.  


## Visualization

With using InfluxDB it is easy to use Grafana (open source) for visualizing the 
current bandwidth. By creating a *Data Source* of type InfluxDB and creating a 
*Dashboard* with the new data source a real 'monitor' can be created. It's 
advised to make that dashboard (or the whole instance) available locally/VPN.

![Sample Grafana Dashboard][grafana-dashboard]

[grafana-dashboard]: sample_dashboard.png "Sample Bandwidth Monitor"
