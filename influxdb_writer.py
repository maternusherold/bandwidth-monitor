#!/usr/bin/env python3
# -*- coding: utf-8

"""Facilitating writer functionality for InfluxDB. """


import configparser
from influxdb import InfluxDBClient
from typing import List


class InfluxDBWriter:
    def __init__(self, path_to_config: str = './config.ini'):
        """Instantiating a writer object to InfluxDB.

        :param path_to_config: config path for users and passwords
        """
        self.config = path_to_config
        self.db_address = None
        self.db_port = None
        self.database = None
        self.user = None
        self.password = None
        self.db_client = None
        self.parse_config()
        self.init_client()

    def parse_config(self):
        """Parsing config file to set database access. """
        config = configparser.ConfigParser()
        config.read(self.config)
        self.db_address = config.get('DATABASE', 'HOST')
        self.db_port = config.get('DATABASE', 'PORT')
        self.database = config.get('DATABASE', 'DB')
        self.user = config.get('DATABASE', 'USERNAME')
        self.password = config.get('DATABASE', 'PASSWORD')

    def init_client(self):
        """Initializes a database client. """
        self.db_client = InfluxDBClient(host=self.db_address, port=self.db_port,
                                        username=self.user, password=self.password,
                                        database=self.database)

    def write_to_database(self, data: List[dict]):
        """Writes provided data to database.

        :param data: JSON formatted input data
        """
        self.db_client.write_points(data)
