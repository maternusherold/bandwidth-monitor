#!/usr/bin/env python
# -*- coding: utf-8

"""Executing speedtests in regular intervals to monitor internet bandwidth.

Depending on several external factors and desire speedtests can be executed
within fixed size intervals. This allows for monitoring and identifying bottlenecks.
ATTENTION: By using a third-party service provided by Speedtest.net hardware,
software and network information will be shared with the service provider.
"""

import os
import speedtest
import time

from monitor_utils import append_measurement, write_to_influxdb


class Monitor:
    def __init__(self, interval: int = 900,
                 save_to_file: str = './bandwidth_stats.csv',
                 measurement_writing_function: callable = append_measurement,
                 threads_to_use: int = 1):
        """Bandwidth monitor executing bandwidth tests in regular intervals.

        :param interval: interval size in seconds; recommended >= 900
        :param save_to_file: path to file
        :param measurement_writing_function: function to write the measurement
        :param threads_to_use: available threads to use for measurement
        """
        self.rate = interval
        self.file_path = os.path.abspath(save_to_file)
        self.measurement_writing_function = measurement_writing_function
        self.threads = threads_to_use
        self.speed_test = speedtest.Speedtest()

    def take_measurement(self, provided_threads: int = 1):
        """Measures down- and up-load speed.

        :param provided_threads: threads to use to measurement
        """
        timestamp = time.time()
        self.speed_test.download(threads=provided_threads)
        self.speed_test.upload(threads=provided_threads)
        return self.speed_test.results.dict(), timestamp

    def monitor_bandwidth(self):
        """Controls measurement rate. """
        while True:
            test_start = time.time()
            # TODO: change to logging
            print('[INFO] Running speedtest on %s' % time.strftime("%a, %d %b %Y "
                                                                   "%H:%M:%S",
                                                                   time.gmtime()))
            data, timestamp = self.take_measurement(provided_threads=self.threads)
            self.write_measurement(data, timestamp=timestamp)
            test_end = time.time()
            test_duration = test_end - test_start
            if test_duration > self.rate:
                continue
            else:
                time.sleep(self.rate - test_duration)

    def write_measurement(self, measurement: dict, timestamp: time.time):
        """Writes measurement to disk.

        :param measurement: measurement data as dict
        :param timestamp: timestamp of measurement
        """
        self.measurement_writing_function(measurements=measurement,
                                          timestamp=timestamp,
                                          path=self.file_path)


if __name__ == '__main__':
    # TODO: add CLI
    monitor = Monitor(measurement_writing_function=write_to_influxdb)
    monitor.monitor_bandwidth()
