#!/usr/bin/env python3
# -*- coding: utf-8

"""Utility functions provided for monitoring internet bandwidth. """

import csv
import os
import time
from typing import List
from influxdb_writer import InfluxDBWriter


def create_directory(path: str = './bandwidth_stats.csv'):
    """
    Creates directory to path if not present.

    :param path: path including file
    """
    file_name = os.path.dirname(path)
    if not os.path.exists(file_name):
        os.makedirs(file_name, exist_ok=True)


def create_file(path: str = './bandwidth_stats.csv',
                fields: List[str] = None):
    """
    Creates a new csv file for temperature measurements.

    :param fields: fields for the new file to be created
    :param path: path to save the file to incl. file name
    """
    if fields is None:
        fields = ['Timestamp', 'Download', 'Upload', 'Ping', 'Server_City',
                  'Server_Country', 'Server_Host', 'Server_Bytes_Sent',
                  'Server_Bytes_Received']

    # creates path directories if not already present
    create_directory(path=path)

    with open(path, 'w') as file:
        csv_writer = csv.writer(file, delimiter=',')
        csv_writer.writerow(fields)


def tidy_measurements(measurements: dict, timestamp: time.time):
    """Provides measurements in tidy format. """
    ret = [float(timestamp)]

    fields_to_search = ['download', 'upload', 'ping']
    for field in fields_to_search:
        ret.append(measurements[field])

    server_keys = ['name', 'country', 'host']
    for key, value in measurements['server'].items():
        if key in server_keys:
            ret.append(value)

    fields_to_search = ['bytes_sent', 'bytes_received']
    for field in fields_to_search:
        ret.append(measurements[field])
    return ret


def append_measurement(measurements: dict, timestamp: time.time,
                       path: str = './bandwidth_stats.csv',
                       fields: List[str] = None):
    """
    Writes new measurements to file.

    :param measurements: measurements in the same order as fields
    :param timestamp:
    :param path: path to file
    :param fields: fields indicating the measurements
    """
    measurements = tidy_measurements(measurements, timestamp)
    if not os.path.isfile(path=path):
        create_file(path=path, fields=fields)

    with open(path, 'a') as file:
        csv_writer = csv.writer(file, delimiter=',')
        csv_writer.writerow(measurements)


def transform_data_to_json(data: dict):
    """Transforms data to JSON format required by InfluxDB.

    :param data: dict as provided by speedtest
    """
    bandwidth_data = [{
        "measurement": "internet_bandwidth",
        "tags": {
            "own_host": "RaspberryPi"
        },
        "fields": {
            "download": data['download'],
            "upload": data['upload'],
            "ping": data['ping'],
            "server_city": data['server']['name'],
            "server_country": data['server']['country'],
            "server_host": data['server']['host'],
            "bytes_sent": data['bytes_sent'],
            "bytes_received": data['bytes_received']
        }
    }]
    return bandwidth_data


def write_to_influxdb(measurements: dict, **kwargs):
    """Writes measurements to InfluxDB database.

    :param measurements: data as dict provided by speedtest
    :param kwargs: interface required parameters
    """
    db_client = InfluxDBWriter()
    data_tidy = transform_data_to_json(measurements)
    db_client.write_to_database(data=data_tidy)

